package src.data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.combat.CombatFleetManager;
import com.fs.starfarer.combat.CombatFleetManager.O0;
import java.util.List;


// Works around battle not ending if ship with modules retreats
public class PulseIndustry_ModuleRetreatCleaner extends BaseEveryFrameCombatPlugin  {
    
	public static final String CUSTOM_DATA_KEY = "shared_module_retreat_cleaner_plugin";
	
    protected IntervalUtil interval = new IntervalUtil(0.5f, 0.5f);
    protected CombatEngineAPI engine;
	protected boolean running = true;	// turn off if another such plugin is running
    
    @Override
    public void advance(float amount, List<InputEventAPI> events) {
		if (!running) return;
        interval.advance(amount);
        if (interval.intervalElapsed()) {
            validateDeployedShips(FleetSide.PLAYER);
            validateDeployedShips(FleetSide.ENEMY);
        }
    }
    
    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
		if (engine.getCustomData().containsKey(CUSTOM_DATA_KEY)) {
			//Global.getLogger(this.getClass()).info("Another module retreat cleaner already running, suspending plugin");
			running = false;
		}
		else {
			engine.getCustomData().put(CUSTOM_DATA_KEY, this);
		}
    }
    
    public void validateDeployedShips(FleetSide side) {
        if (engine == null) return;
        
        CombatFleetManager man = (CombatFleetManager)engine.getFleetManager(side);
        boolean anyNonModule = false;
        
        if (man.getDeployed().isEmpty())
            return;
        
        for (O0 deployed : man.getDeployed()) {
            if (!deployed.isStationModule()) {
                anyNonModule = true;
                break;
            }
        }
        if (!anyNonModule) {
           /* Global.getLogger(this.getClass()).info("Clearing orphaned modules from "
                    + "deployed list for side " + side.toString() + ": " + man.getDeployed().size());*/
            man.getDeployed().clear();
        }
    }
 
}
