package src.data.scripts.world;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.ModSpecAPI;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import java.util.Arrays;
import java.util.List;

import src.data.scripts.ai.*;

public class PulseIndustry_ModPlugin extends BaseModPlugin {

    private final String IDAL = "PulseIndustry_al_shot";
    public static final List<String> modAuthorBlacklist = Arrays.asList(new String[] // These all have good reasons
    {
        "xenoargh" // Makes many pseudo total conversions, but are not tagged as such
    });
        public static final List<String> modBlacklist = Arrays.asList(new String[] // These all have good reasons
    {
        "xxx_ss_FX_example_project",
           "xxx_ss_FX_mod_core",
                   "@_ss_rebal_@",
                           "explorer_society",
                                   "ezfaction",
                                           "xxx_Starsector_AI_Overhaul",
    });
    
    @Override
    public void onApplicationLoad() {
        for (ModSpecAPI mod : Global.getSettings().getModManager().getEnabledModsCopy())
        {
            if (modAuthorBlacklist.contains(mod.getAuthor()))
            {
                throw new RuntimeException("" + mod.getName() + " is not compatible with Snrasha mods! (See Snrasha on the Discord)");
            }
           
            if(modBlacklist.contains(mod.getId())){
                throw new RuntimeException("" + mod.getName() + " is not compatible with Snrasha mods! (See Snrasha on the Discord)");
            }
        }
        
        boolean hasLazyLib = Global.getSettings().getModManager().isModEnabled("lw_lazylib");
        if (!hasLazyLib) {
            throw new RuntimeException("Require LazyLib!"
                    + "\nGet it at http://fractalsoftworks.com/forum/index.php?topic=5444");
        }
    }
    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {

        switch (missile.getProjectileSpecId()) {

            case IDAL:
                return new PluginPick<MissileAIPlugin>(new PulseIndustry_CustomAL(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            default:
                return null;
        }
    }
}
